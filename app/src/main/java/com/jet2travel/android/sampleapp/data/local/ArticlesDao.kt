package com.jet2travel.android.sampleapp.data.local

import androidx.lifecycle.LiveData
import androidx.room.*
import com.jet2travel.android.sampleapp.data.entities.Article
import com.jet2travel.android.sampleapp.data.entities.ArticleInfo
import com.jet2travel.android.sampleapp.data.entities.Media
import com.jet2travel.android.sampleapp.data.entities.User

@Dao
interface ArticlesDao {

    @Transaction
    @Query("SELECT * FROM ArticleInfo")
    fun getArticles(): LiveData<List<Article>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertArticles(articles: List<ArticleInfo>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertMediaList(articles: List<Media>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertUsers(articles: List<User>)
}