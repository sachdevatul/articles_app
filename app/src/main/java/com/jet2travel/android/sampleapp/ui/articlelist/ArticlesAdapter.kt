package com.jet2travel.android.sampleapp.ui.articlelist

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.CircleCrop
import com.jet2travel.android.sampleapp.data.entities.Article
import com.jet2travel.android.sampleapp.databinding.ItemArticleBinding
import com.jet2travel.android.sampleapp.utils.ArticleUtils

class ArticlesAdapter :
    RecyclerView.Adapter<ArticleViewHolder>() {
    private val items = ArrayList<Article>()
    fun addItems(items: ArrayList<Article>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder {
        val binding: ItemArticleBinding =
            ItemArticleBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ArticleViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) =
        holder.bind(items[position])
}

class ArticleViewHolder(
    private val itemBinding: ItemArticleBinding
) : RecyclerView.ViewHolder(itemBinding.root) {
    private lateinit var article: Article

    @SuppressLint("SetTextI18n")
    fun bind(item: Article) {
        this.article = item
        if (this.article.users.isNotEmpty()) {
            val user = this.article.users.get(0)

            Glide.with(itemBinding.root)
                .load(user.avatar)
                .transform(CircleCrop())
                .into(itemBinding.userImage)

            itemBinding.username.text = user.name + " " + user.lastname
            itemBinding.userDesignation.text = user.designation
        }
        if (this.article.mediaList.isEmpty()) {
            itemBinding.articlesImage.visibility = View.GONE
        } else {
            itemBinding.articlesImage.visibility = View.VISIBLE
            Glide.with(itemBinding.root)
                .load(this.article.mediaList.get(0).image)
                .into(itemBinding.articlesImage)
        }

        itemBinding.dateTime.text = ArticleUtils.getDateString(this.article.article.createdAt)
        itemBinding.articlesContents.text = this.article.article.content
        itemBinding.articlesCommentsCount.text =
            ArticleUtils.getCommentLikesString(
                this.article.article.comments,
                ArticleUtils.ARTCILE_COMMENTS
            )
        itemBinding.articlesLikesCount.text =
            ArticleUtils.getCommentLikesString(
                this.article.article.likes,
                ArticleUtils.ARTCILE_LIKES
            )
    }
}
