package com.jet2travel.android.sampleapp.utils

import java.util.*

class ArticleUtils {
    companion object {
        val ARTCILE_COMMENTS: String= "Comments";
        val ARTCILE_LIKES: String= "LIKES";

        val MS_IN_HOUR: Long = 3600000L;
        val MS_IN_DAY: Long = 86400000L;
        fun getDateString(date: Date): String {
            if (date.time > (System.currentTimeMillis() - MS_IN_DAY)) {
                return ((System.currentTimeMillis() - date.time) / MS_IN_HOUR).toString() + " Hours";
            } else {
                return ((System.currentTimeMillis() - date.time) / MS_IN_DAY).toString() + " Days";
            }
        }

        fun getCommentLikesString(value: Int, type: String): String {
            if (value > 1000000) {
                val number:Double = value.toDouble() / 1000000;
                return String.format("%.1f", number) + "M $type";
            } else if (value > 1000) {
                val number:Double = value.toDouble() / 1000;
                return String.format("%.1f", number) + "K $type";
            } else {
                return (value).toString() + " $type";
            }
        }
    }
}