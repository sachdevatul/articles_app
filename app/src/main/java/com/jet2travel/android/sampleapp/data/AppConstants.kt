package com.jet2travel.android.sampleapp.data

object AppConstants {
    const val DB_NAME: String = "articles_db"

    // SERVER Constants
    const val SERVER_URL: String = "https://5e99a9b1bc561b0016af3540.mockapi.io/jet2/api/v1/"
    const val API_BLOGS: String = "blogs"
    const val PAGE: String = "page"
    const val LIMIT: String = "limit"
}