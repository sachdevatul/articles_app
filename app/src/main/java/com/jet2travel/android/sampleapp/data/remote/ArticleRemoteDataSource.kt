package com.jet2travel.android.sampleapp.data.remote

import javax.inject.Inject

class ArticleRemoteDataSource @Inject constructor(
    private val articleService: ArticleService
) : BaseDataSource() {

    suspend fun getArticles(page: Int, limit: Int) =
        getResult { articleService.getArticles(page, limit) }
}