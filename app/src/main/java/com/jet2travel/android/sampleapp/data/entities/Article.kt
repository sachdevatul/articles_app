package com.jet2travel.android.sampleapp.data.entities

import androidx.room.Embedded
import androidx.room.Relation
import androidx.room.RoomWarnings
import com.google.gson.annotations.SerializedName
import java.util.*

data class Article(
    @Embedded
    var article: ArticleInfo,

    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @SerializedName("id")
    val articleId: Int?,
    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @SerializedName("createdAt")
    val articleCreatedAt: Date?,
    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @SerializedName("content")
    val articleContent: String?,
    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @SerializedName("comments")
    val articleComments: Int?,
    @SuppressWarnings(RoomWarnings.CURSOR_MISMATCH)
    @SerializedName("likes")
    val articleLikes: Int?,

    @Relation(parentColumn = "id", entityColumn = "blogId")
    @SerializedName("media")
    val mediaList: List<Media>,

    @Relation(
        parentColumn = "id",
        entityColumn = "blogId"
    )
    @SerializedName("user")
    val users: List<User>
) {
    fun getArticleInfo(): ArticleInfo {
        article = ArticleInfo(
            articleId!!,
            articleCreatedAt!!,
            articleContent!!,
            articleComments!!,
            articleLikes!!
        )
        return article
    }
}