package com.jet2travel.android.sampleapp.data.remote

import com.jet2travel.android.sampleapp.data.AppConstants
import com.jet2travel.android.sampleapp.data.entities.Article
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ArticleService {
    @GET(AppConstants.API_BLOGS)
    suspend fun getArticles(
        @Query(AppConstants.PAGE) page: Int,
        @Query(AppConstants.LIMIT) limit: Int
    ): Response<List<Article>>
}