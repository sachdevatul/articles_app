package com.jet2travel.android.sampleapp.data.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class Media(
    @PrimaryKey
    @SerializedName("id")
    val id: Int,
    @SerializedName("blogId")
    val blogId: Int,

    @SerializedName("createdAt")
    val createdAt: Date,

    @SerializedName("image")
    val image: String,

    @SerializedName("title")
    val title: String,

    @SerializedName("url")
    val url: String
)