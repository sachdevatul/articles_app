package com.jet2travel.android.sampleapp.data.entities

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class User(
    @PrimaryKey
    @SerializedName("id")
    val id: Int,
    @SerializedName("blogId")
    val blogId: Int,

    @SerializedName("createdAt")
    val createdAt: Date,

    @SerializedName("name")
    val name: String,

    @SerializedName("avatar")
    val avatar: String,

    @SerializedName("lastname")
    val lastname: String,

    @SerializedName("city")
    val city: String,

    @SerializedName("designation")
    val designation: String,

    @SerializedName("about")
    val about: String
)