package com.jet2travel.android.sampleapp.di

import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jet2travel.android.sampleapp.data.AppConstants
import com.jet2travel.android.sampleapp.data.local.AppDatabase
import com.jet2travel.android.sampleapp.data.local.ArticlesDao
import com.jet2travel.android.sampleapp.data.remote.ArticleRemoteDataSource
import com.jet2travel.android.sampleapp.data.remote.ArticleService
import com.jet2travel.android.sampleapp.data.repository.ArticleRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import dagger.hilt.android.qualifiers.ApplicationContext
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideRetrofit(gson: Gson): Retrofit = Retrofit.Builder()
        .baseUrl(AppConstants.SERVER_URL)
        .addConverterFactory(GsonConverterFactory.create(gson))
        .build()

    @Provides
    fun provideGson(): Gson = GsonBuilder().create()

    @Provides
    fun provideArticleService(retrofit: Retrofit): ArticleService =
        retrofit.create(ArticleService::class.java)

    @Singleton
    @Provides
    fun provideArticleRemoteDataSource(
        articleService: ArticleService
    ) = ArticleRemoteDataSource(articleService)

    @Singleton
    @Provides
    fun provideDatabase(@ApplicationContext appContext: Context) =
        AppDatabase.getDatabase(appContext)

    @Singleton
    @Provides
    fun provideArticleDao(db: AppDatabase) = db.atriclesDao()

    @Singleton
    @Provides
    fun provideRepository(
        remoteDataSource: ArticleRemoteDataSource,
        localDataSource: ArticlesDao
    ) =
        ArticleRepository(remoteDataSource, localDataSource)
}