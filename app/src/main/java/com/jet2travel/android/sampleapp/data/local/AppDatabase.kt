package com.jet2travel.android.sampleapp.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.jet2travel.android.sampleapp.data.AppConstants
import com.jet2travel.android.sampleapp.data.entities.*

@Database(
    entities = [ArticleInfo::class, Media::class, User::class],
    version = 1,
    exportSchema = false
)
@TypeConverters(DateTypeConverters::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun atriclesDao(): ArticlesDao

    companion object {
        @Volatile
        private var instance: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase =
            instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }

        private fun buildDatabase(appContext: Context) =
            Room.databaseBuilder(appContext, AppDatabase::class.java, AppConstants.DB_NAME)
                .fallbackToDestructiveMigration()
                .build()
    }

}