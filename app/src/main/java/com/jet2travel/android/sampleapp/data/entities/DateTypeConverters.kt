package com.jet2travel.android.sampleapp.data.entities

import androidx.room.TypeConverter
import java.util.*

class DateTypeConverters {
    @TypeConverter
    public fun fromTimestamp(value: Long?): Date? {
        return value?.let { Date(it) }
    }

    @TypeConverter
    public fun dateToTimestamp(date: Date?): Long? {
        return date?.time?.toLong()
    }
}