package com.jet2travel.android.sampleapp.data.entities

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.util.*

@Entity
data class ArticleInfo(
    @PrimaryKey
    val id: Int,
    val createdAt: Date,
    val content: String,
    val comments: Int,
    val likes: Int
)