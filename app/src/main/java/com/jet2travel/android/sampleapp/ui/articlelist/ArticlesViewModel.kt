package com.jet2travel.android.sampleapp.ui.articlelist

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.switchMap
import com.jet2travel.android.sampleapp.data.entities.Article
import com.jet2travel.android.sampleapp.data.repository.ArticleRepository
import com.jet2travel.android.sampleapp.utils.Resource

class ArticlesViewModel @ViewModelInject constructor(
    private val repository: ArticleRepository
) : ViewModel() {
    private val _page = MutableLiveData<Int>()
    private val LIMIT = 10

    init {
        _page.value = 1;
    }

    private val _articles = _page.switchMap { page ->
        repository.getArticles(page, LIMIT)
    }

    val articles: LiveData<Resource<List<Article>>> = _articles

    fun loadNextPage() {
        _page.value = _page.value?.toInt()!! + 1
    }
}
