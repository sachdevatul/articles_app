package com.jet2travel.android.sampleapp.data.repository

import com.jet2travel.android.sampleapp.data.entities.ArticleInfo
import com.jet2travel.android.sampleapp.data.entities.Media
import com.jet2travel.android.sampleapp.data.entities.User
import com.jet2travel.android.sampleapp.data.local.ArticlesDao
import com.jet2travel.android.sampleapp.data.remote.ArticleRemoteDataSource
import com.jet2travel.android.sampleapp.utils.performGetOperation
import javax.inject.Inject

class ArticleRepository @Inject constructor(
    private val remoteDataSource: ArticleRemoteDataSource,
    private val localDataSource: ArticlesDao
) {

    fun getArticles(page: Int, limit: Int) = performGetOperation(
        databaseQuery = { localDataSource.getArticles() },
        networkCall = { remoteDataSource.getArticles(page, limit) },
        saveCallResult = {
            val articleInfos: ArrayList<ArticleInfo> = ArrayList<ArticleInfo>()
            val mediaList: ArrayList<Media> = ArrayList<Media>()
            val users: ArrayList<User> = ArrayList<User>()

            it.forEach {
                articleInfos.add(it.getArticleInfo())
                if(it.mediaList != null)
                mediaList.addAll(it.mediaList)

                if(it.users != null)
                users.addAll(it.users)
            }
            localDataSource.insertArticles(articleInfos)
            localDataSource.insertMediaList(mediaList)
            localDataSource.insertUsers(users)
        }
    )
}